<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    use HasFactory;

    public const ORDER_TYPES = [
        '0' => 'не сортувати',
        '1' => 'за ФІО студента',
        '2' => 'за темою'
    ];


    public $incrementing = true;
    public $timestamps = false;

    public function data_query(){
        return DB::table('students as s')
            ->join('themes as t','t.theme_id','=','s.theme')
            ->select('*');
    }


    public function index(){
        return $this->data_query()
            ->get();
    }

    protected $fillable = [
        'student_fio',
        'raiting',
        'curator_fio',
        'theme'

    ];
}
