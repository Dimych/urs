<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{

    public function showList()
    {
        return view('list', ['data' => (new Student)->index()]);
    }

    public function index(Request $request)
    {
        $query = (new Student)->data_query();

        if ($request->filled('c_fio'))
            $query->where('curator_fio', 'LIKE', '%' . $request->c_fio . '%');

        switch ($request->order) {
            case '1':
                $query->orderBy('student_fio');
                break;

            case '2':
                $query->orderBy('theme');
                break;

            default:
                break;
        }

        return view('admin.view', ['request' => $request, 'data' => $query->get()]);
    }

    public function create()
    {
        return view('admin.add', ['themes' => \App\Models\Theme::all()]);
    }

    public function store(Request $request)
    {
        $student = new Student();

        $student->student_fio = $request->s_fio;
        $student->rating = $request->rating;
        $student->curator_fio = $request->c_fio;
        $student->theme = $request->theme;


        $student->save();

        return Redirect::to('/students');
    }


    public function show(Student $student)
    {
        $query = (new Student)->data_query()->where('id', $student->id)->get();
        return view('student', ['data' => $query]);
    }


    public function edit(Student $student)
    {
        return view('admin.edit', ['data' => $student, 'themes' => \App\Models\Theme::all()]);
    }


    public function update(Request $request, Student $student)
    {
        $student->student_fio = $request->s_fio;
        $student->rating = $request->rating;
        $student->curator_fio = $request->c_fio;
        $student->theme = $request->theme;


        $student->save();

        return Redirect::to('/students');
    }

    public function destroy(Student $student)
    {
        $student->delete();
        return Redirect::to('/students');
    }
}
