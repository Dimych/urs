<h1>Головна сторінка</h1>

<table border="1">

    <a href="/students">Адмін-панель</a>

    <tr>
        <td>ID</td>
        <td>ФІО студента</td>
        <td>Зарахований бал</td>
        <td>ФІО викладача</td>
        <td>Тема</td>

    </tr>
    @foreach($data as $obj)
        <tr>
            <td>{{$obj->id}}</td>
            <td>{{$obj->student_fio}}</td>
            <td>{{$obj->rating}}</td>
            <td>{{$obj->curator_fio}}</td>
            <td>{{$obj->theme_title}}</td>

        </tr>
    @endforeach
</table>
<br>
