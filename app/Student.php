<?php


namespace App;


class Student
{
public $id;
public $student_fio;
public $curator_fio;
public $theme;
public $rating;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStudentFio()
    {
        return $this->student_fio;
    }

    /**
     * @param mixed $student_fio
     */
    public function setStudentFio($student_fio): void
    {
        $this->student_fio = $student_fio;
    }

    /**
     * @return mixed
     */
    public function getCuratorFio()
    {
        return $this->curator_fio;
    }

    /**
     * @param mixed $curator_fio
     */
    public function setCuratorFio($curator_fio): void
    {
        $this->curator_fio = $curator_fio;
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme): void
    {
        $this->theme = $theme;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating): void
    {
        $this->rating = $rating;
    }

}
