<h1>Адмінка</h1>
<form class="form-group">
    <span>Пошук за викладачем: <input type="text" name="c_fio" placeholder="ФІО викладача"></span>
    <span>Сортувати:
        <select name="order">
            @foreach(\App\Models\Student::ORDER_TYPES as $key => $type)
                <option value="{{$key}}" {{($request->order == $key) ? 'selected' : ''}}>{{$type}}</option>
            @endforeach
        </select>
    </span>
    <input type="submit" value="Пошук">
</form>
<table class="table_price" border="1" >
    <tr>
        <td>ID</td>
        <td>ФІО студента</td>
        <td>Зарахований бал</td>
        <td>ФІО викладача</td>
        <td>Тема</td>

    </tr>
    @foreach($data as $obj)
        <tr>
            <td>{{$obj->id}}</td>
            <td><a href="/students/{{$obj->id}}">{{$obj->student_fio}}</a></td>
            <td>{{$obj->rating}}</td>
            <td>{{$obj->curator_fio}}</td>
            <td>{{$obj->theme_title}}</td>
            <td ><a href="/students/{{$obj->id}}/edit">Edit</a></td>
            <form action="{{route('students.edit',[$obj->id])}}" method="post">
                @csrf
                @method('DELETE')
                <td >
                    <input type="submit" value="Delete">
                </td>
            </form>
        </tr>
    @endforeach
</table>
<br>
<a href="/students/create">Додати дипломну роботу</a><br><br>
<a href="/">Головна сторінка</a>
