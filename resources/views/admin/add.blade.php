<h1>Додати дипломну роботу</h1>

<form action="/students" method="post">
    @csrf
    <span>ФІО Студента: <input type="text" name="s_fio" placeholder="ФІО" required></span><br><br>
    <span>ФІО Викладача: <input type="text" name="c_fio" placeholder="ФІО" required></span><br><br>
    <span>Тема дипломної роботи:
        <select name="theme">
        @foreach($themes as $theme)
                <option value="{{$theme->theme_id}}">{{$theme->theme_title}}</option>
            @endforeach
    </select>
    </span><br><br>
    <span>Рейтинг: <input type="number" name="rating" min="0" max="100" required></span><br><br>
    <input type="submit" value="Додати">
</form>
