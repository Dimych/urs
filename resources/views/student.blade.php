<h1>Інформація про дипломну роботу:</h1>
<table border="1">
    <tr>
        <td>ID</td>
        <td>ФІО студента</td>
        <td>ФІО викладача</td>
        <td>Тема</td>
        <td>Зарахований бал</td>
    </tr>
    <tr>
    <tr>
        @foreach($data as $el)
            <td>{{$el->id}}</td>
            <td>{{$el->student_fio}}</td>
            <td>{{$el->curator_fio}}</td>
            <td>{{$el->theme_title}}</td>
            <td>{{$el->rating}}</td>
            @endforeach
    </tr>
</table>
<br>
<a href="/students">Повернутись назад</a>
