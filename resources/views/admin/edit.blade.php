<h1>Редагування</h1>

<form action="/students/{{$data->id}}" method="post">
    @csrf
    @method('PUT')
    <span>ФІО Студента: <input type="text" name="s_fio" value="{{$data->student_fio}}" required></span><br><br>
    <span>ФІО Викладача: <input type="text" name="c_fio" value="{{$data->curator_fio}}" required></span><br><br>
    <span>Тема дипломної роботи:
        <select name="theme">
           @foreach($themes as $theme)
                <option value="{{$theme->theme_id}}">{{$theme->theme_title}}</option>
            @endforeach
    </select>
    </span><br><br>
    <span>Рейтинг: <input type="number" name="rating" value="{{$data->rating}}" required></span><br><br>
    <input type="submit" value="Редагувати">
</form>
