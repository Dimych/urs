<?php

namespace Tests\Unit;

use App\Student;
use PHPUnit\Framework\TestCase;

class StudentTest extends TestCase
{
    protected function setUp(): void
    {
        $this->student = new Student();
    }
    /**
     * @dataProvider rateDataProvider
     * @param $actual
     * @param $expected
     */

    public function testRating($actual, $expected)
    {
        $result = $this->rating = $actual;
        $this->assertEquals($expected, $result);
    }

    public function rateDataProvider(): array
    {
        return array(
            array(100, 100),
            array(85, 85),
            array(93, 93)
        );
    }
    protected function tearDown(): void
    {
    }
    public function testMock()
    {
        $mock = $this->getMockBuilder(Student::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $mock->method('getTheme')
            ->willReturn('Інформаційна система пошта');

        $this->assertSame('Інформаційна система пошта', $mock->getTheme());
    }
}
